package com.juan.opentrends.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.juan.opentrends.Objects.Entity;
import com.juan.opentrends.Objects.Person;
import com.juan.opentrends.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Asus on 21/11/2015.
 */
public class CustomListview extends RecyclerView.Adapter<CustomListview.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Entity item);
    }
    private final OnItemClickListener listener;
    private List<Entity> entityList;

    public CustomListview( List<Entity> entityList, OnItemClickListener listener) {
        this.listener = listener;
        this.entityList = entityList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_person, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(entityList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return entityList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text) TextView text;
        @BindView(R.id.image) ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(final Entity item, final OnItemClickListener listener) {

            if(item != null)
            {
                Person person = item.person;
                text.setText(""+person.description);
                if(person.thumbnail != null )
                    Picasso.with(itemView.getContext()).load(person.thumbnail).into(image);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

}