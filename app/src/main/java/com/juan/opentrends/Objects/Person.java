package com.juan.opentrends.Objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Asus on 21/11/2015.
 */
public class Person {

    @SerializedName("gender")
    public String gender;

    @SerializedName("number")
    public String number;

    @SerializedName("thumbnail")
    public String thumbnail;

    @SerializedName("picture")
    public String picture;

    @SerializedName("descritpion")
    public String description;

}
