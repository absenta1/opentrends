package com.juan.opentrends;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.juan.opentrends.Fragments.ListPersonFragment;
import com.juan.opentrends.Objects.Person;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public ListPersonFragment listPersonFragment;

    public FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        if(savedInstanceState == null)
        {
            listPersonFragment = new ListPersonFragment(this);
            replaceFragment(listPersonFragment);
        }
    }

    /**
     * Método encargado de reemplazar un fragmento
     * @param frag
     */
    public void replaceFragment(Fragment frag) {

        FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.container, frag);
        ft.commit();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
