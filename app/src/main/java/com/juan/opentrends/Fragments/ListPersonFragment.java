package com.juan.opentrends.Fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.juan.opentrends.Adapters.CustomListview;
import com.juan.opentrends.MainActivity;
import com.juan.opentrends.Objects.Entity;

import com.juan.opentrends.R;
import com.juan.opentrends.Services.Service;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Asus on 21/11/2015.
 * Fragmento encargado de presentar la información de una persona
 */
public class ListPersonFragment extends Fragment {

    private View view;
    public CustomListview adapter;
    public @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public MainActivity mainActivity;

    public ListPersonFragment() {
    }

    @SuppressLint("ValidFragment")
    public ListPersonFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.list_person, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getPersons();
    }

    /**
     * Metodo que pide la información al servicio
     */
    public void getPersons() {
        OkHttpClient okHttpClient = new OkHttpClient();
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(mainActivity.getString(R.string.load_info));
        progressDialog.setCancelable(false);
        progressDialog.show();

        Request request = new Request.Builder()
                .url(Service.GET_INFO)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                progressDialog.dismiss();
                Looper.prepare();
                Toast.makeText(getActivity(), R.string.error_service, Toast.LENGTH_SHORT).show();
                Looper.loop();
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                progressDialog.dismiss();
                final String result = response.body().string();
                if (response.isSuccessful()) {

                    Looper.prepare();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject json = new JSONObject(result);
                                Entity[] entities = new Gson().fromJson(json.getString("results"),Entity[].class);
                                ArrayList<Entity> entities1  = new ArrayList<Entity>(Arrays.asList(entities));
                                loadPersonInListview(entities1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    Looper.loop();
                }
            }


        });
    }
    /**
     * Metodo que cargar las personas en la lista y añade el listener a la fila
     *
     * @param entities
     */
    public void loadPersonInListview(ArrayList<Entity> entities) {

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mainActivity);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new CustomListview(entities, new CustomListview.OnItemClickListener() {
            @Override public void onItemClick(Entity entity) {
                DetailPersonFragment detailPersonFragment = new DetailPersonFragment(entity.person);
                detailPersonFragment.show(mainActivity.fragmentManager,"DetailPersonFragment.TAG");
            }
        });
        recyclerView.setAdapter(adapter);
    }
}
