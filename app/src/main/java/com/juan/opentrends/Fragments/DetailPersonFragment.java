package com.juan.opentrends.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.juan.opentrends.Objects.Person;
import com.juan.opentrends.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Asus on 21/11/2015.
 */
public class DetailPersonFragment  extends DialogFragment implements View.OnClickListener{

    private View view;
    private Person person;
    @BindView(R.id.imageView) ImageView image;
    @BindView(R.id.text) TextView text;
    @BindView(R.id.progressBar)ProgressBar progressBar;
    @BindView(R.id.imgBackDetail)ImageView imgBackDetail;

    public DetailPersonFragment(){}

    @SuppressLint("ValidFragment")
    public DetailPersonFragment(Person person)
    {
        this.person = person;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_person,container,false);
        ButterKnife.bind(this,view);
        imgBackDetail.setOnClickListener(this);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_Holo_Light_DarkActionBar);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Se carga la información detallada de la persona
        if(person != null)
        {
            text.setText(""+person.description);
            if(person.picture != null)
                Picasso.with(getActivity()).load(person.picture).into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        progressBar.setVisibility(View.GONE);
                    }
                });

        }
    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.imgBackDetail:
                dismiss();
                break;
            default:
                break;
        }
    }
}
